<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cadastro de usu�rio</title>
</head>
<body style="background-color: #FFFF00;">


	<h1>Cadastro de Usu�rio</h1>


	<form action="salvarUsuario" method="post">
		<table>

			<tr>

				<td>C�digo:</td>
				<td><input type="text" readonly="readonly" id="id" name="id"
					value="${user.id} " style="border-radius: 5px;"></td>

			</tr>
			<tr>

				<td>Login:</td>
				<td><input type="text" id="login" name="login"
					value="${user.login} " style="border-radius: 5px;"></td>

			</tr>
			<tr>

				<td>Senha:</td>
				<td><input type="password" id="senha" name="senha"
					value="${user.senha}" style="border-radius: 5px;"></td>

			</tr>

		</table>
		<input type="submit" value="Salvar">
	</form>

	<table>
		<c:forEach items="${usuarios}" var="user">
			<tr>
				<td style="width: 150px"><c:out value=" ${user.id}" /></td>
				<td style="width: 150px"><c:out value=" ${user.login}" /></td>
				<td style="width: 150px"><c:out value=" ${user.senha}" /></td>
				<td><a href="salvarUsuario?acao=delete&user=${user.login}">Excluir</a></td>
				<td><a href="salvarUsuario?acao=editar&user=${user.login}">Editar</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>